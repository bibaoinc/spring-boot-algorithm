package com.bibao.boot.algorithm.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class BubbleSort implements GenericSort {

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c) {
		sort(list, c, true);
	}

	@Override
	public <T> void sort(List<T> list, Comparator<? super T> c, boolean ascending) {
		if (list==null || list.size()<=1) return;
		int n = list.size();
		int ascendingFlag = ascending? 1 : -1;
		while (n>0) {
			int newn = 0;
			for (int i=0; i<n-1; i++) {
				if (c.compare(list.get(i), list.get(i+1)) * ascendingFlag > 0) {
					Collections.swap(list, i, i+1);
					newn = i + 1;
				}
			}
			n = newn;
		}
	}

}
