package com.bibao.boot.algorithm.dp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class LCS {
	
	public List<String> longestCommonSubstring(String first, String second) {
		int n = first.length();
		int m = second.length();
		int z = 0; 									// Keep the max length of the common substring
		int[][] array = new int[n+1][m+1];
		Set<String> result = new HashSet<>();		// Keep all common non-duplicates substrings with length z
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=m; j++) {
				if (first.charAt(i-1)==second.charAt(j-1)) {
					if (i==1 || j==1) array[i][j] = 1;
					else array[i][j] = array[i-1][j-1] + 1;
				}
				if (array[i][j]>z) {
					z = array[i][j];
					result.clear();
					result.add(first.substring(i-z, i));
				} else {
					if (array[i][j]==z) {
						result.add(first.substring(i-z, i));
					}
				}
			}
		}
		return new ArrayList<>(result);
	}
	
	public int longestCommonSubsequence(String first, String second) {	// Subsequence may not be adjacent
		int n = first.length();
		int m = second.length();
		int[][] array = new int[n+1][m+1];
		for (int i=1; i<=n; i++) {
			for (int j=1; j<=m; j++) {
				if (first.charAt(i-1)==second.charAt(j-1)) {
					array[i][j] = array[i-1][j-1] + 1;
				} else {
					array[i][j] = Math.max(array[i-1][j], array[i][j-1]);
				}
				
			}
		}
		return array[n][m];
	}
}
