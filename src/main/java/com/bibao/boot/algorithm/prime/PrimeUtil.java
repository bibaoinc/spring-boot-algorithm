package com.bibao.boot.algorithm.prime;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PrimeUtil {
	@Value("${prime.size}")
	private int size;
	
	public List<Integer> getPrimes() {
		List<Integer> primeList = new ArrayList<>();
		primeList.add(2);
		int count = 1;
		int currentOdd = 3;
		while (count<size) {
			if (isPrime(currentOdd)) {
				primeList.add(currentOdd);
				count++;
			}
			currentOdd += 2;
		}
		return primeList;
	}
	
	private boolean isPrime(int odd) {
		for (int i=2; i<=odd-1; i++) {
			if (odd%i==0) return false;
		}
		return true;
	}
	
	public List<Integer> getPrimesEfficient() {
		List<Integer> primeList = new ArrayList<>();
		primeList.add(2);
		int count = 1;
		int currentOdd = 3;
		while (count<size) {
			if (isPrimeEfficient(currentOdd)) {
				primeList.add(currentOdd);
				count++;
			}
			currentOdd += 2;
		}
		return primeList;
	}
	
	private boolean isPrimeEfficient(int odd) {
		int m = (int)Math.sqrt(odd);
		for (int i=3; i<=m; i+=2) {
			if (odd%i==0) return false;
		}
		return true;
	}
	
	public List<Integer> getPrimesCache() {
		List<Integer> primeList = new ArrayList<>();
		primeList.add(2);
		int count = 1;
		int currentOdd = 3;
		while (count<size) {
			if (isPrimeCache(currentOdd, primeList)) {
				primeList.add(currentOdd);
				count++;
			}
			currentOdd += 2;
		}
		return primeList;
	}
	
	private boolean isPrimeCache(int odd, List<Integer> cachedPrimes) {
		int m = (int)Math.sqrt(odd);
		for (int prime: cachedPrimes) {
			if (odd%prime==0) return false;
			if (prime>m) break;
		}
		return true;
	}
}
