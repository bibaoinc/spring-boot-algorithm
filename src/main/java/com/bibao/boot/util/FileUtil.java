package com.bibao.boot.util;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;

@Component
public class FileUtil {
	@Value("${file.root.path}")
	private String rootPath;
	
	public void writePrimes(List<Integer> primeList, String filename) {
		try (FileWriter file = new FileWriter(rootPath + filename);
			PrintWriter out = new PrintWriter(file)) {
			int count = 0;
			for (int prime: primeList) {
				out.printf("%8d", prime);
				count++;
				if (count%10==0) out.println();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
