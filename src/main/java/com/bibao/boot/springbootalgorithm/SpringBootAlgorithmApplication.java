package com.bibao.boot.springbootalgorithm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.bibao.boot")
public class SpringBootAlgorithmApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAlgorithmApplication.class, args);
	}

}

