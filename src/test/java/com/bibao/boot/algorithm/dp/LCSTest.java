package com.bibao.boot.algorithm.dp;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.springbootalgorithm.SpringBootAlgorithmApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootAlgorithmApplication.class})
public class LCSTest {
	@Autowired
	private LCS lcs;
	
	@Test
	public void testLongestCommonSubstring() {
		List<String> result = lcs.longestCommonSubstring("abacdeb", "deacb");
		assertEquals(2, result.get(0).length());
		Collections.sort(result);
		assertEquals("ac", result.get(0));
		assertEquals("de", result.get(1));
	}

	@Test
	public void testLongestCommonSubsequence() {
		assertEquals(5, lcs.longestCommonSubsequence("acdsadcbaa", "deabcaad"));
	}
}
