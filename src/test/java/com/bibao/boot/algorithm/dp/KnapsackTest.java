package com.bibao.boot.algorithm.dp;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bibao.boot.springbootalgorithm.SpringBootAlgorithmApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootAlgorithmApplication.class})
public class KnapsackTest {
	@Autowired
	private Knapsack knapsack;
	
	@Test
	public void testMaxValue() {
		int[] values = {100, 150, 200, 220};
		int[] weights = {20, 25, 30, 45};
		assertEquals(370, knapsack.maxValue(values, weights, 70));		// {25, 45}
		assertEquals(450, knapsack.maxValue(values, weights, 75));		// {20, 25, 30}
		assertEquals(300, knapsack.maxValue(values, weights, 50));		// {20, 30}
	}

}
